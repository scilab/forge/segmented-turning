function [r,v]=CheckHeights(pheight)
//Checks the answer given in Edit uicontrol for ring heights
//It should be evaluated as a real scalar in [bounds(1), bounds(2)] interval
//It should be evaluated as a real scalar in [bounds(1), bounds(2)] interval
  
  h=gcbo;
  if execstr("v=(["+h.string+"])","errcatch")<>0 then
    RE_SetError(h,%t,_("TS","Expression cannot be evaluated"))
    r=%f,v=[]
  elseif type(v)<>1|~isreal(v)|or(v<=0) then
    RE_SetError(h,%t,_("TS","positive scalars expected"))
    r=%f
  else
    v=matrix(v,1,-1);
    if size(v,"*")==1 then
      v=v*ones(1,ceil(pheight/v))
      r=%t
    else
      r=sum(v)>=pheight
      if r then
        RE_SetError(h,~r)//un hilite edit area
      else
        msg=msprintf(_("TS","Sum of heights is less than profile height=%g"),pheight)
        RE_SetError(h,~r,msg)
      end
    end
  end
endfunction
