function TS_Cuts(D,fn,t)
//Generates a csv file with description of all cuts
  if argn(2)<3 then t=2;end
  with_stave=%f
  for l=1:size(D.H,'*')
    if D.RingsProperties(l).Type=="Staves" then 
      with_stave=%t;
    end
  end
  ext=fileparts(fn,"extension");
  xls_export=or(ext==[".xls" ".xlsx"])
  //miter angle (Cut angle degree) blade tilt for staves
  head_std=[_("TS","Ring index"),...
            _("TS","Wood type"),...
            _("TS","Exterior diameter"),...
            _("TS","Number of segments"),...
            _("TS","Exterior length mm"),...
            _("TS","Miter angle degree"),...
            _("TS","Fence position mm"),...
            _("TS","Board''s height mm"),...
            _("TS","Board''s width mm"),...
            _("TS","Board''s length mm")];
  
  
  head_staves=[_("TS","Ring index"),...
               _("TS","Wood type"),...
               _("TS","Max exterior diameter"),...
               _("TS","Number of segments"),...
               _("TS","Staves'' max width mm"),...
               _("TS","Miter angle degree"),...
               _("TS","Blade tilt degree"),...
               _("TS","Splay angle degree"),...
               _("TS","Board'' height mm"),...
               _("TS","Board''s thickness mm"),...
               _("TS","Board''s length mm")]      

  T_std=[]
  T_staves=[]
  for l=1:size(D.H,'*')
    RP=D.RingsProperties(l);
    H=D.H(l);

    select RP.Type
    case "Standard" then
      Re=D.Re(l)+D.Ewidth;
      Ri=max(0,D.Ri(l)-D.Ewidth);
      
      T_std=[T_std;StandardCuts(l,Re,Ri,H,RP)];
    case "Zigzag" then
      Re=D.Re(l)+D.Ewidth;
      Ri=max(0,D.Ri(l)-D.Ewidth);
      T_std=[T_std;ZigzagCuts(l,Re,Ri,H,RP)];
    case "Staves" then      
      T_staves=[T_staves;StavesCuts(l,H,RP)]
    end
  end
  T=[];
  if xls_export then //
    copyfile(TS_Path()+"/etc/Cut_template"+ext,fn)
    if T_std<>[] then
      T_std=[num2cell(head_std)
             T_std]
      xlwrite(fn,T_std,1,"A1")
    end
    if T_staves<>[] then
      T_staves=[num2cell(head_staves)
         T_staves]
      xlwrite(fn,T_staves,2,"A1")
    end
  else//csv export
    if T_std<>[] then
      T=[T;
         strcat(head_std,";")
         T_std]
    end
    if T_staves<>[] then
      if T_std<>[]  then T=[T;""];end
      T=[T;_("TS","Staves")]
      
      T=[T;
         strcat(head_staves,";")
         T_staves]
    end
    deletefile(fn);
    mputl(T,fn)
  end
endfunction

