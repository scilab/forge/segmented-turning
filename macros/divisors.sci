function r=divisors(n)
//returns in r the array of the integer divisors of the integer n
  if n==1 then r=1; return; end;
  f=factor(n);
  fu=unique(f);
  r=1;
  for z=fu
    nf=length(find(z==f));
    s=[1 z.^(1:nf)];
    r=r.*.s;
  end
  r=gsort(r,"g","i");
endfunction


