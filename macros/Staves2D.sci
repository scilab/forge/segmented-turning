function [X,Y]=Staves2D(D,l)
H=D.H;

Hl=H(l);
RP=D.RingsProperties(l)
N=RP.N
A=%pi/N;
StavesProps=RP.StavesProps(1)
Re=StavesProps.Re
ai=-diff(Re)/Hl;//pente des faces ext et int
SplayAngle=%pi/2-abs(atan(ai));
MiterCutAngle=atan(sin(SplayAngle)*tan(A));//Miter angle
 
TiltCutAngle=atan(tan(A).*cos(atan(tan(SplayAngle)*(1./cos(A)))));

MiterCutAngle=A
Ri=StavesProps.Ri*cos(MiterCutAngle);

Ri=max(0,Ri-D.Ewidth);
Re=Re+D.Ewidth;

X=[Ri(1);Re(1);Re(2);Ri(2);Ri(1)];
Y=sum(H(1:l-1))+[0;0;Hl;Hl;0];

endfunction
