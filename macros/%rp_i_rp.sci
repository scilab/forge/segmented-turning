function dest=%rp_i_rp(i,src,dest)
  fn=fieldnames(dest)'
  for f=fn;
    if type(src(f))==15 then
      df=dest(f)
      df(i)=src(f)(1)
      dest(f)=df
    else
      dest(f)(i)=src(f);
    end
  end
 
endfunction
