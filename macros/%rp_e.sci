function rp=%rp_e(i,rp)
//Extracts the properties of the ith ring out of all Ring properties
  fn=fieldnames(rp)';
  for f=fn
    if type(rp(f))==15 then
      rp(f)= list(rp(f)(i))
    else
      rp(f)= rp(f)(i)
    end
  end
endfunction
