function e=drawvalue(x,y,v,unit)
  t=string(round(100*v)/100)+unit;
  rect=xstringl(0,0,t);
  w=rect(3)/2;
  h=rect(4)/2;
  xstring(x-w,y-h,t)
  e=gce()
endfunction
