function r=RE_SetError(h,r,msg)
//Show erroneous edited value in uicontrol.
//erroneous input is high lighted in red and an message is shown in the
//bottom of the RingEditor window.
  if ~r then 
    h.backgroundcolor=[1 1 1]
    h.parent.parent.info_message=""
  else
    h.backgroundcolor=[1 0 0]
    if argn(2)==3 then 
      h.parent.parent.info_message=msg
      //mprintf(msg+"\n");
    end
  end
endfunction
