function expr=index2exp(ind)
  //Converts a vector of indexes into a Scilab expression trying to
  //minimize the expression length
  expr=[];
  while ind<>[]
    d=diff(ind)
    k=find(d<>d(1),1)
    if k==2 then //try to optimize
       d=diff(ind(2:$))
       k1=find(d<>d(1),1)
       if k1>1|k1==[] then k=1;end
    end  
    if k==[] then k=size(ind,"*");end
    if k==1 then
      expr=[expr string(ind(1))]
    elseif k==2 then
       expr=[expr string(ind(1)) string(ind(k))]
    else
      if d(1)==1 then
        expr=[expr string(ind(1))+":"+string(ind(k))]
      else
        expr=[expr string(ind(1))+":"+string(d(1))+":"+string(ind(k))]
      end
    end
    ind(1:k)=[]
  end
  if size(expr,"*")>1 then
    expr="["+strcat(expr,",")+"]"
  end
endfunction
