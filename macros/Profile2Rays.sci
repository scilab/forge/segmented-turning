function D=Profile2Rays(D)
//Computes the interior and exterior ring rays given the interior and
//exterior profiles in D.Pi and D.Pe.
//The computed rays are returned in D.Ri and D.Re 
  Ewidth=D.Ewidth;
  Pe=D.Pe
  Pi=D.Pi
  H=D.H
 
  nl=size(H,"*")
  h=[0 cumsum(H(:))'];
  n=size(h,'*');
  Re=[];Ri=[];
  
  k=find(diff(Pe.y)<=0)
  if k<>[] then
    Pe.y(k)=[];
    Pe.x(k)=[];
  end
  y=0:ceil(max(Pe.y))
  
  x=interp1(Pe.y,Pe.x,y,"linear")
 
  for l=1:nl
    k=find(y>=h(l)&y<=h(l+1));
    if k==[] then
      k=find(y>=h(l),1)
      if k==[] then //out of bounds ring
         Re=[Re Re($)];
      else
        if k==size(y,"*") then k=k-1;end
        yy=linspace(h(l),h(l+1),10)
        xx=interpln([y(k) y(k+1);x(k) x(k+1)],yy)
        k=find(yy<=h(l+1),1);
        Re=[Re xx(k)];
      end
    else
      Re=[Re max(x(k))];
    end
  end
  
  k=find(diff(Pi.y)<=0)
  if k<>[] then
    Pi.y(k)=[];
    Pi.x(k)=[];
  end
  y=floor(min(Pi.y)):ceil(max(Pi.y))
  x=interp1(Pi.y,Pi.x,y,"linear")
 
  for l=1:nl
    k=find(y>=h(l)&y<=h(l+1));
    if k==[] then
      k=find(y>=h(l),1)
      if k==[] then //out of bounds ring
         Ri=[Ri Ri($)];
      else
        if k==size(y,"*") then k=k-1;end
        yy=linspace(h(l),h(l+1),10)
        xx=interpln([y(k) y(k+1);x(k) x(k+1)],yy)
        k=find(yy<=h(l+1),1)
        Ri=[Ri xx(k)];
      end
    else
      Ri=[Ri min(x(k))];//
    end
  end
  Ri(1)=0;
  Ri=max(Ri,0);
  D.Re=ceil(Re)
  D.Ri=floor(Ri)
endfunction
