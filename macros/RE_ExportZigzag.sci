function RE_ExportZigzag(win)
//Filemenu callback for the "Export zigzag" sub menu    
//it generates a scaled PDF file 
  if type(win)==1 then
     mainH=findobj("figure_id", win)
  else
    mainH=win
  end
  ud=mainH.user_data
  D=ud.D
  H=ud.H
  l=H.index.value
  RP=D.RingsProperties(l);
  if RP.Type<>"Zigzag" then 
    messagebox(_("TS","The current ring is not zigzag"),"message","error","modal")
    return
  end
  path=uigetfile("*.pdf","",msprintf(_("TS","Give zigzag#%d export file"),l));
  driver("PDF")

  xinit(path)
  f=scf(max(winsid())+1);
  ZZ_Design(D,l)
  ax=f.children
  ax.background=-2
  xend()
 
  driver("Rec")
  scf(mainH)
endfunction
