function [x1,y1]=Rot2D(x,y,teta,orig)
//Realizes a 2D rotation

// x,y   :real arrays with same sizes (the coordinates)
// teta : rotation angle in radiant
// orig : coordinates of the center of  rotation, [0;0] is omitted.
  if x==[] then x1=[];y1=[];return;end
  [lhs,rhs]=argn(0)
  if rhs<4 then orig=[0;0];end
  
  //
  [m,n]=size(x);
  if or(size(y)<>[m,n]) then
    error(msprintf(_("%s and %s must have same size.\n"),"x","y"))
  end
  x=x-orig(1);
  y=y-orig(2);
  //
  
  c=cos(teta),s=sin(teta)
  x1=orig(1)+(c*x-s*y);
  y1=orig(2)+(s*x+c*y);
  
endfunction
