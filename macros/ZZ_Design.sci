function ZZ_Design(D,lr)
  RP=D.RingsProperties(lr);
  zigzagprops=RP.ZigzagProps(1)
  Re=D.Re(lr)+D.Ewidth;
  Ri=max(0,D.Ri(lr)-D.Ewidth);
  N=RP.N;
  h=D.H(lr);
  A=%pi/N;
  zh=zigzagprops.zheight
  thickness=zigzagprops.thickness
  thickness=[thickness($:-1:1) thickness(2:$)];
  C=colors(zigzagprops.colors)
  C=[C($:-1:1) C(2:$)];
  Le=Re*tan(A);
 
  
  dh=(h-zh)/2
  
  e=sum(thickness(2:$-1))
  t=(e*zh+Le*sqrt(zh^2+Le^2-e^2))/(zh^2+Le^2);
  a=atan((zh*t-e)/Le,t);
  thickness([1 $])=Le*sin(a)+dh
  th=[0 cumsum(thickness)];


  n=size(thickness,"*")
  h1=e/cos(a);
  
  drawlater()
  ax=gca();delete(ax.children)
  ax.axes_bounds(4)=0.45
 
  //laminated board
  ax=gca();ax.view="2d";ax.axes_visible="off";ax.isoview="on";ax.tight_limits=["on","on"];
  ax.background=color("lightgray");
  ax.margins=ax.margins/1.2;
  T=msprintf(_("TS","Ring %d zigzag design"),lr)
 
 
  L=2*Le/cos(a)+th($)*tan(a);
  x=ones(1,n).*.[0;L;L;0];
  y=[th(1:$-1);th(1:$-1);th(2:$);th(2:$)];
  xfpolys(x,y,C)
  E=ceil(10*(Re-Ri.*cos(A)))/10;
  Lb=ceil(N*(2*Le/cos(a)+2)+th($)*tan(a));
  s=msprintf(_("TS","Laminated board:\n   witdh=%g mm,\n   length=%g mm"),E, ...
             Lb);
 
  drawstring(L/2,th($),[T;"";s])
  ax.data_bounds=[0 0;L th($)];
  
  wx=Le/5;wy=wx;
  //Laminated board cuts
  ncuts=3;
  x=[0;th($)*tan(a)]*ones(1,ncuts)+ones(2,1)*(0:(ncuts-1))*(Le/cos(a))
  y=[0;th($)]*ones(1,ncuts)
  xpolys(x,y,color("red")*ones(1,ncuts));gce().children.thickness=2;
  //cotes
  for k=1:n
    cotev([0 th(k)],-wy,thickness(k),"")
  end
  coteh([0 0],-wx,Le/cos(a),"mm")
  coteh([Le/cos(a) 0],-wx,Le/cos(a),"mm")
  cotea([Le/cos(a) 0],[0 %pi/2-a],wx)
 
  ax.data_bounds=[-wx -wy;L th($)];


  ax1=newaxes()
  ax1.axes_bounds=ax.axes_bounds;
  ax1.axes_bounds(2)=0.5;
  ax1.isoview="on";ax1.axes_visible="off";ax1.tight_limits=["on","on"];
   x=[];y=[];
  //left
  y0=-thickness(1)/cos(a);
  y1=y0+Le*tan(a)
  for k=1:n
    h1=thickness(k)/cos(a);
    x=[x;   0 Le    Le   0  0]; 
    y=[y; [y0 y1 y1+h1 y0+h1 y0]];
    y0=y0+h1;
    y1=y1+h1;
  end
  //right
  y0=-thickness(1)/cos(a);
  y1=y0+Le*tan(a)
 
  for k=1:n
    h1=thickness(k)/cos(a);
    x=[x;   Le+[Le 0 0 Le  Le]];
    y=[y; [y0 y1 y1+h1 y0+h1 y0]];
    y0=y0+h1;
    y1=y1+h1;
  end
  mny=min(y);
 
  xfpolys(x',y'-mny,[C C])
 
  //cuts
  xc=[0 0;
       2*Le 2*Le];
  yc=[y(2,1)-dh h-dh 
      y(2,1)-dh h-dh]-mny;
 
  xpolys(xc,yc,color("red")*[1 1])
  gce().children.thickness=2;
  //cotes
  
  cotev([0,yc(1,2)],-wy,yc(1,1),"")
  cotev([0,0],-wy,yc(1,1),"")
  cotev([0,yc(1,1)],-wy,h,"")
  coteh([0,0],-wx,2*Le,"mm")

  
  ax1.data_bounds=[-wx+min(x) -wy; ax.data_bounds(2,1) max(y)-mny];
  ax.data_bounds(2,2)=ax1.data_bounds(2,2)
  ax1.background=color("lightgray");
  ax1.margins=ax.margins;

  drawnow()
endfunction
