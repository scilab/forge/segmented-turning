function RE_DemosMenu(mainH)
//Creates the File menu for the RingEditor window
  w=string(mainH.figure_id);
 
  Demos=uimenu("parent", mainH, "label", _("TS","Demonstrations"),"Handle_Visible", ...
              "off","callback" ,list(4,"RE_Demos("+w+")"))
 
endfunction
