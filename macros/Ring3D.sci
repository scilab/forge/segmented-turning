function [S,M]=Ring3D(D,l,view)
//Computes and draw if requested the (turned) 3d view of the lth ring of
//the workpiece
  if argn(2)<3 then
    view=%f
  elseif and(view<>[%t %f]) then
    error(msprintf(_("%s: Wrong type for argument #%d: Boolean expected.\n"),"Ring3d",3))
  end
  X=[];Y=[];Z=[];C=[];
  RP=D.RingsProperties(l);
  N=RP.N
  h=cumsum([0 D.H]);
  xef=D.Pe.x;
  zef=D.Pe.y;
  xif=D.Pi.x;
  zif=D.Pi.y;
  k=find(diff(zif)==0);
  if k<>[] then
    xif(k)=[];zif(k)=[];
  end
  zz=linspace(min(D.Pe.y),max(D.Pe.y),300);
 
  xef=interpln([zef;xef],zz);zef=zz;
  xif=interpln([zif;xif],zz);zif=zz;

  na=size(D.H,"*");
  dh=h($)/50
  k=find(zef>h(l)&zef<=h(l+1));
  if k==[] then 
    return
  end
  n=max(2,ceil((zef(k($))-zef(k(1)))/dh))
  nk=size(k,"*");
  if nk>n then
    k=k(1:floor(size(k,"*")/n):$)
  end
  Re_z=xef(k);ze=zef(k);
  if l<na &ze($)<>h(l+1) then 
    if n>1 then Re_z($)=Re_z($)+(h(l+1)-ze($))*(Re_z($)-Re_z($-1))/(ze($)-ze($-1));end
    ze($)=h(l+1);
  end
  if ze(1)<>h(l) then 
    if n>1 then Re_z(1)=Re_z(1)+(h(l)-ze(1))*(Re_z(1)-Re_z(2))/(ze(1)-ze(2));end
    ze(1)=h(l);
  end
  if l==1 then  
    Ri_z=zeros(ze);
  else 
    i=find(ze>zif(1),1);
    Ri_z=[zeros(1,i-1),interpln([zif;xif],ze(i:$))];
  end
  
  select RP.Type
  case "Standard" then
    [S,M]=StandardRing3D(N,Ri_z,Re_z,ze,RP)
  case "Zigzag" then
    [S,M]=ZigzagRing3D(N,Ri_z,Re_z,ze,RP)
  case "Staves"
    [S,M]=StandardRing3D(N,Ri_z,Re_z,ze,RP)
  end
 
  if view then
    drawlater()
    ax=gca();delete(ax.children);
    Surf3D(S,M)
    xtitle(msprintf(_("TS","Ring number %d"),l))
    drawnow()
  end
endfunction
