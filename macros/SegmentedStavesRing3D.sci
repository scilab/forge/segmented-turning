function [X,Y,Z,C]=SegmentedStavesRing3D(D,l)
  X=[];Y=[];Z=[];C=[];
  RP=D.RingsProperties(l);
  H=D.H;
  Hl=H(l)
  h=sum(H(1:l-1))
  col=colors(RP.Colors(1));
  N=RP.N;
  GapProps=RP.GapProps(1)
  ngap=GapProps.ngap
  gap=GapProps.gap
  typ=convstr(GapProps.type)
  filled=GapProps.filled
 
  StavesProps=RP.StavesProps(1)
  Ri=StavesProps.Ri;
  Re=StavesProps.Re

  A=%pi/N;
  SplayAngle=atan(diff(Re)/Hl)
  MiterCutAngle=atan(sin(SplayAngle)*tan(A));//Miter angle
  TiltCutAngle=atan(ones(SplayAngle)*tan(A).*cos(atan(tan(SplayAngle)*(1./cos(A)))))

  a=SplayAngle;
  Ri=max(0,Ri-D.Ewidth/cos(a));
  Re=Re+D.Ewidth/cos(a);

  [x,y,z]=Stave3D(Ri,Re,N,Hl,GapProps)
  gap_index=[]
  if ngap==0|gap==0|typ=="full"  then
    //Stave3D return a single "segment"
    //no gap, all segments have the same shape
    nfs=size(x,2)
    A=%pi/N
    a=0;
    for k=1:N
      [x,y]=Rot2D(x,y,a,[0,0]);
      X=[X x];
      Y=[Y y];
      Z=[Z z];
      a=2*A;
    end
  elseif ngap==N then
     //Stave3D returns 2 "segments" in X,Y,Z 
      //the first one is the segment 
      //the second one if the spacer
      a=0
      for k=1:N
        [x,y]=Rot2D(x,y,a,[0,0]);
        if filled then gap_index=[gap_index size(X,2)+6+(1:6)];end
        X=[X x];
        Y=[Y y];
        Z=[Z z];
        a=2*%pi/N ;
      end
  else
    //Stave3D returns 4 "segments" in X,Y,Z 
    //the first one is the segment upon spacer
    //the second one is the segment above spacer
    //the third one is the regular segment
    //the last one is the spacer if any
    ns=size(x,2)/6;
    i=13:18
    [xr,yr,zr]=(x(:,i),y(:,i),z(:,i));
    i=1:12;
    if filled then i=[i 19:24];end
    [x,y,z]=(x(:,i),y(:,i),z(:,i));
    a=2*%pi/N ;
    b=0;
    for l=1:ngap
      [x,y]=Rot2D(x,y,b)
      if filled then gap_index=[gap_index size(X,2)+12+(1:6)];end
      X=[X x];Y=[Y y];Z=[Z,z];
      for k=1:(N-2*ngap)/ngap     
        [xr,yr]=Rot2D(xr,yr,a)
        X=[X xr];Y=[Y yr];Z=[Z,zr];
        a=2*%pi/N ;
      end
      b=2*%pi/ngap
      a=6*%pi/N ;
    end
  end
  Z=Z+h;
  nf=size(X,2);
  ind=1:nf;
  ind(gap_index)=[];
  C=zeros(1,nf);
  nfs=6
 
  C(ind)=matrix(ones(nfs,1)*col,1,-1);
  if gap_index<>[] then
    C(gap_index)=colors(RP.SpacerColor)
  end

endfunction
