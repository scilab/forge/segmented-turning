function guiHandle=RingEditor(D)
//Creates the Ring Ediror interactive window
//If the D argument is given it contains the workpiece data structure to
//be edited
//without input argument it creates the window with an empty
//workpiece. The workpiece can then be loaded.
  margin_x     = 5;      // Horizontal margin between each elements
  margin_y     = 5;      // Vertical margin between each elements
  button_w     = 140;
  button_h     = 30;
  label_h      = 25;
  label_w      = 180;
  bg=color("lightgray");
  defaultfont  = "arial"; 
  guiHandle = figure("menubar", "none", ...
                     "toolbar","none", ...
                     "infobar_visible", "on",  ...
                     "axes_size",[900 600],...
                     "figure_name",_("TS","Ring editor")+" (%d)",...
                     "visible","off",...
                     "event_handler","RE_Events",...
                     "event_handler_enable", "on",...
                     "color_map",wood_colormap(),...
                     "tag","Editor");
  guiHandle.closerequestfcn="RE_Close("+string(guiHandle.figure_id)+")";
  guiHandle.background=color("lightgray")
  ax=gca();
  ax.axes_bounds=[0.35 0 0.65 1];
  RE_FileMenu(guiHandle)
  RE_ToolsMenu(guiHandle)
  RE_HelpMenu(guiHandle)
  RE_DemosMenu(guiHandle)
  
  frame_border=createBorder("titled",createBorder("etched"),...
                            _("TS","Select the ring properties"),...
                            "left","top",createBorderFont("Sans Serif",12,"bold"),"blue")
 
  H=[];

  exec(ringgui,-1);
  exec(stdgui,-1);
  exec(displaygui,-1)
  exec(zzgui,-1);
  
 
  guiHandle.user_data=struct("H",H,"D",[],"Path","Untitled.ts","Edited",%f);
  guiHandle.visible="on"
  if argn(2)==1 then
    RE_Update(guiHandle,D)
    RE_callback()
  end
  guiHandle.visible="on"
endfunction

function ringgui
  wf1=310;hf1=170;
  xf1=margin_x;yf1=600-hf1-4*margin_y;
  f1=uicontrol( ...
      "parent"              , guiHandle,...
      "style"               , "frame",...
      "position"            , [xf1 yf1 wf1 hf1],....
      "fontsize"            , 12,...
      "fontWeight"          , "bold",...
      "background"          , 0.85*ones(1,3),...
      "border"              , frame_border,...
      "tag"                 , "RE_RP");
  x=margin_x;y=hf1-4*margin_y-label_h;
  CreateLabel(f1,x,y,label_w-10,label_h,_("TS","Ring index"));
  x=x+margin_x+label_w-10;
  H.index=CreatePopup(f1,x,y,50,label_h,["1";"2"],"index","RE_callback()");

  x=margin_x;
  y=y-margin_y-label_h;
  CreateLabel(f1,x,y,label_w,label_h,_("TS","Ring height mm"));
  x=x+margin_x+label_w;
  H.RH=CreateEdit(f1,x,y,40,label_h,"","RH","RE_callback()");
 
  x=margin_x;
  y=y-margin_y-label_h;
  CreateLabel(f1,x,y,label_w,label_h,_("TS","Number of segments"));
  x=x+margin_x+label_w;
  H.N=CreateEdit(f1,x,y,40,label_h,"","N","RE_callback()");
 
  x=margin_x;
  y=y-margin_y-label_h;
  CreateLabel(f1,x,y,label_w,label_h,_("TS","Rotation angle"));
  x=x+margin_x+label_w;
  H.Rot=CreateEdit(f1,x,y,40,label_h,"","Rot","RE_callback()");

  x=margin_x;
  y=y-margin_y-label_h;
  CreateLabel(f1,x,y,0.7*label_w,label_h,_("TS","Ring type"));
  x=x+margin_x+0.7*label_w;
  choices=[_("TS","Standard"),_("TS","Zigzag"),_("TS","Staves")];
  //choices=[_("TS","Standard"),_("TS","Zigzag")];
  H.RingType=CreatePopup(f1,x,y,0.9*label_w,label_h,choices,"RingType","RE_callback()");
  H.RingType.value=1;
endfunction

function stdgui
  frame_border(3)= _("TS","Select the segments properties");
  wf2=wf1;hf2=300;
  xf2=margin_x;yf2=yf1-hf2-margin_y;
  fstd=uicontrol( ...
      "parent"              , guiHandle,...
      "style"               , "frame",...
      "position"            , [xf2 yf2 wf2 hf2],....
      "fontsize"            , 12,...
      "fontWeight"          , "bold",...
      "background"          , 0.85*ones(1,3),...
      "border"              , frame_border,...
      "tag"                 , "RE_STD");
  //colors
  x=margin_x;
  x=margin_x;y=hf2-4*margin_y-label_h;
  
  CreateLabel(fstd,x,y,0.7*label_w,label_h,_("TS","Segment wood"));
  x=x+margin_x+0.7*label_w;
  [C,Cn]=wood_colormap();
  hc=rgb2hex(C);
  choices=[hc,Cn,hc];
  H.Color=CreatePopup(fstd,x,y,0.9*label_w,label_h,choices,"color","RE_callback()");
  x=margin_x;
  y=y-margin_y-2*label_h;
  CreateLabel(fstd,x,y,0.7*label_w,label_h,_("TS","Segment indices"));
  x=x+margin_x+0.7*label_w;
  H.nc=CreateEdit(fstd,x,y,0.9*label_w,2*label_h,"[]","nc","RE_callback()");
 
  
  x=margin_x;
  y=y-margin_y-label_h;
  CreateLabel(fstd,x,y,label_w,label_h,_("TS","Number of spaces"));
  x=x+margin_x+label_w-10;
  H.ngap=CreatePopup(fstd,x,y,50,label_h,"0","ngap","RE_callback()");


  x=margin_x;
  y=y-margin_y-label_h;
  CreateLabel(fstd,x,y,label_w,label_h,_("TS","Spacer thickness mm"));
  x=x+margin_x+label_w-10;
  H.gap=CreateEdit(fstd,x,y,50,label_h,"","gap","RE_callback()");

  x=margin_x;
  y=y-margin_y-label_h;
  CreateLabel(fstd,x,y,0.7*label_w,label_h,_("TS","Spacing type"));
  x=x+margin_x+0.7*label_w;
  choices=[_("TS","Spacer"),_("TS","Opened")];
  H.Type=CreatePopup(fstd,x,y,0.9*label_w,label_h,choices,"Type","RE_callback()");
  
  x=margin_x;
  y=y-margin_y-label_h;
  CreateLabel(fstd,x,y,0.7*label_w,label_h,_("TS","Spacer wood"));
  x=x+margin_x+0.7*label_w;
  [C,Cn]=wood_colormap();
  hc=rgb2hex(C);
  choices=[hc,Cn,hc];
  H.scolor=CreatePopup(fstd,x,y,0.9*label_w,label_h,choices,"scolor","RE_callback()");
 
  x=margin_x;
  y=y-margin_y-2*label_h;
  CreateLabel(fstd,x,y,0.7*label_w,label_h,_("TS","Apply to:"));
  x=x+margin_x+0.7*label_w;
  H.ApplyTo=CreateEdit(fstd,x,y,0.9*label_w,2*label_h,"[]","ApplyTo","RE_callback()");
endfunction

function zzgui
  wf2=wf1;hf2=325;
  xf2=margin_x;yf2=yf1-hf2-margin_y;
  frame_border(3)= _("TS","Select the zigzag properties");
  fzz=uicontrol( ...
      "parent"              , guiHandle,...
      "style"               , "frame",...
      "position"            , [xf2 yf2 wf2 hf2],....
      "fontsize"            , 12,...
      "fontWeight"          , "bold",...
      "background"          , 0.85*ones(1,3),...
      "border"              , frame_border,...
      "visible"            ,"off",...
      "tag"                 , "RE_ZZ");

 // ud=mainH.user_data;
 // Handles=ud.H;
  
  // D=ud.D;
  // l=Handles.index.value;
  // RP=D.RingsProperties(l);
  // Re=D.Re(l);
  // SegmentProps=[D.H(l),2*Re*tan(%pi/RP.N)];
 
  x=margin_x;
  y=hf2-4*margin_y-label_h;
  CreateLabel(fzz,x,y,label_w,label_h,_("TS","Zigzag height mm"));
  x=x+margin_x+label_w;
  H.zheight=CreateEdit(fzz,x,y,40,label_h,"","zheight","RE_callback()");
   
  x=margin_x;
  y=y-margin_y-label_h;
  CreateLabel(fzz,x,y,0.7*label_w,label_h,_("TS","Layers thickness mm"));
  x=x+margin_x+0.7*label_w;
  H.thickness=CreateEdit(fzz,x,y,0.9*label_w,label_h,"[]","thickness","RE_callback()");
 
  x=margin_x;
  y=y-margin_y-label_h;
  CreateLabel(fzz,x,y,label_w-10,label_h,_("TS","Layer index"));
  x=x+margin_x+label_w-10;
  H.ZZindex=CreatePopup(fzz,x,y,50,label_h,"1","ZZindex","RE_callback()");
  H.ZZindex.value=0;
  x=margin_x;
  y=y-margin_y-label_h;
  CreateLabel(fzz,x,y,0.7*label_w,label_h,_("TS","Layer wood"));
  x=x+margin_x+0.7*label_w;
  [C,Cn]=wood_colormap()
  hc=rgb2hex(C);
  choices=[hc,Cn,hc];
  H.ZZColor=CreatePopup(fzz,x,y,0.9*label_w,label_h,choices,"ZZcolor","RE_callback()");
 
endfunction


function displaygui
  frame_border(3)= _("TS","Select the display");
  wf3=wf1;hf3=150;
  xf3=margin_x;yf3=yf2-hf3-margin_y;
  f3=uicontrol( ...
      "parent"              , guiHandle,...
      "style"               , "frame",...
      "position"            , [xf3 yf3 wf3 hf3],....
      "fontsize"            , 12,...
      "fontWeight"          , "bold",...
      "background"          , 0.85*ones(1,3),...
      "border"              , frame_border,...
      "tag"                 , "RE_DSP");
  
 

  //Display
  x=margin_x;
  y=hf3-4*margin_y-label_h;
  CreateLabel(f3,x,y,0.7*label_w,label_h,_("TS","Display type"));
  x=x+margin_x+0.7*label_w;
  choices=[_("TS","2D view"),_("TS","Raw Ring 2D"),...
           _("TS","Raw Ring 3D"),_("TS","Turned Ring 3D"),...
           _("TS","Raw workpiece"),_("TS","Turned workpiece"),...
           _("TS","Unrolled"),...
          _("TS","Element sketch")];
  H.Display=CreatePopup(f3,x,y,0.9*label_w,label_h,choices,"Display","RE_callback()");
  H.Display.value=6;
endfunction
