function D=StavesRays(D,l)
  H=D.H;
  Pi=D.Pi;
  Pe=D.Pe;
  RP=D.RingsProperties(l)
  N=RP.N;
  A=%pi/N
  h=[0 cumsum(H)];
  yi=Pi.y;xi=Pi.x;
  ye=Pe.y;xe=Pe.x;


  ki=find(yi>=h(l)&yi<=h(l+1));
  y=linspace(h(l),h(l+1),50);
  xe=interpln([ye;xe],y);
  xi=interpln([yi;xi],y);
  //compute the equation of the line going from interior first and last
  //points
  ai=(xi(1)-xi($))/(y(1)-y($));
  bi=(y(1)*xi($)-y($)*xi(1))/(y(1)-y($));
  //x=ai*y+bi
  //translate the line to make all profile points on its right
  e=xi-(ai*y+bi)
  k=find(e<0)
  if k<>[] then bi=bi+min(e);end
  //The exterior limit is supposed to be a parallel to the line with all
  //exterior profile points on its left 
  e=max(xe-(ai*y+bi))
  be=bi+e;
  st=struct("Re",ai*[y(1);y($)]+be,"Ri",(ai*[y(1);y($)]+bi))
  RP.StavesProps(1)=st;
  D.RingsProperties(l)=RP;
endfunction
