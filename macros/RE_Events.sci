function RE_Events(win, x, y, ibut)
  if and(ibut<>[3 4]) then return;end
   mainH=findobj("figure_id", win)
   Handles=mainH.user_data.H
   fn=Handles.Display.string(Handles.Display.value)
   if fn<>_("TS","Unrolled") then return;end
   ax=gca();
   cur_wood=ax.user_data
   [x,y]=xchange(x,y,'i2f');
   D=mainH.user_data.D;
   Heights=D.H./D.Re;
   h=[0 cumsum(Heights)]
   l=find(y>=h(1:$-1)&y<h(2:$))//Ring index
   if l==[] then return;end
   
   RP=D.RingsProperties(l);
   N=RP.N;
   if RP.Type=="Zigzag" then return;end
   
   C=RP.Colors(1)
   rot=sum(D.RingsProperties.Rotation(1:l));
   nshift=int(rot)
   ind=pmodulo(-nshift+(0:N-1),N)+1

   Cs=ax.children(2:2:$)
   Segs=Cs($-l+1)
   d=Segs.children.data
   X=d($:-4:4,1)
   W=d($-1:-4:3,1)-X
   k=find(x>=X&x<X+W);

   if k==[] then return;end
   if ibut==4&cur_wood<>[] then //middle button copy current wood
      C(ind(k))=cur_wood
   else
     [RGB,Cn]=wood_colormap()
     i=x_choose(Cn,_("TS","Choose wood"))
     if i==0 then return;end
     C(ind(k))=rgb2hex(RGB(i,:))
     ax.user_data=C(ind(k))
   end
   Segs.children($+1-k).background=colors(C(ind(k)))
   RP.Colors(1)=C
   D.RingsProperties(l)=RP
   mainH.user_data.Edited=%t
   mainH.user_data.D=D
endfunction
