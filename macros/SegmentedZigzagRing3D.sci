function [X,Y,Z,Col]=SegmentedZigzagRing3D(D,lr)
  Re=D.Re(lr)+D.Ewidth;
  Ri=max(0,D.Ri(lr)-D.Ewidth);
  H=D.H
  h=[0 cumsum(H)](lr)
  RP=D.RingsProperties(lr);
  N=RP.N;
  height=D.H(lr);
  A=%pi/N;
  p=Re-Ri*cos(A);
  l=2*Re*tan(A);
  //A importer depuis les donnees generee par ZigzagEditor
  zigzagprops=RP.ZigzagProps(1)

  if zigzagprops.thickness==[] then 
    return
  end
  [X,Y,Z,Col]=ZigzagSegment3D(Ri,Re,N,height,zigzagprops);
  
  //Add segments around
  n=size(Y,2);
  for k=1:N-1
    prev=$-n+1:$
    [x,y]=Rot2D(X(:,prev),Y(:,prev),-2*A,[0 0])
    X=[X x];Y=[Y y];Z=[Z Z(:,prev)];Col=[Col Col(prev)];
  end
  Col=colors(Col)
 
  Z=Z+h
endfunction
