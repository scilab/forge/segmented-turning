function l=CreateLabel(parent,x,y,w,h,label)
   l=uicontrol( ...
      "parent"              , parent,...
      "style"               , "text",...
      "string"              , label,...
      "position"            , [x,y,w,h ],....
      "fontsize"            , 12,...
      "horizontalalignment" , "left", ...
      "background"          , 0.85*ones(1,3),...
      "visible"             , "on");
endfunction
