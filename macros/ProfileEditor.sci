function guiHandle=ProfileEditor(caller);
//Interactive profil editor
if argn(2)<1 then caller=[];end
      
// See help page for usage description.  

  z=[];
  guiHandle = figure("menubar", "none", ...
                     "infobar_visible", "on",  ...
                     "event_handler","PE_Events",...
                     "event_handler_enable", "off",...
                     "figure_name",_("TS","Profile editor")+" (%d)",...
                     "visible","off",...
                     "tag","PEditor");
  
  guiHandle.figure_position(2)=30;

  guiHandle.closerequestfcn="PE_Close("+string(guiHandle.figure_id)+")";
  ax=gca();
  ax.margins=0.05*ones(1,4);
  ax.isoview="on";
  ax.tight_limits=["on","on"];
  xgrid()
  D=120;H=100; //Default workpiece Diameter and Height
  sz=get(0, "screensize_px")(3:4);
  guiHandle.figure_size=(sz(2)-60)*[D/2/H 1];
  ax.data_bounds=[-1 -1;D/2 H];

  // noeuds du profil  en cours d'edition
  plot(0,0,'b+-');c1=gce().children;// force initial point to (0,0)
  //Profil en cours d'edition
  plot(0,0,'r');c2=gce().children;c2.data=[];
  //Tracé du profil non edité
  plot(0,0,'b');c3=gce().children;c3.data=[];
  //profil interieur par defaut
  plot(0,0,'k');c4=gce().children;c4.data=[];
  //sauvegarde des noeuds du profil non edité
  plot(0,0);c5=gce().children;c5.data=[];c5.visible="off";
  C=[c1 c2 c3 c4 c5]
  guiHandle.user_data=struct("Path","Untitled.pf",...
                             "C",C,"pile",[],...
                             "D",D,"H",H,...
                             "thickness",10,...
                             "Active",1,...
                             "move",[],"caller",caller)
  PE_FileMenu(guiHandle)
  PE_ToolsMenu(guiHandle)
 
  guiHandle.figure_position = [10 10]
  guiHandle.visible="on";
  guiHandle.event_handler_enable="on";
 endfunction


