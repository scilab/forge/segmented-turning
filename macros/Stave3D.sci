function [X,Y,Z]=Stave3D(Ri,Re,N,H,GapProps)
//https://www.youtube.com/watch?v=1xlV1tutDxw
//vertices index
//    7  .------. 3
//     / |    / |
//    /  |   /  |
//   /  8|  /   |
//6 .----.-.--- .4
//  |   /  |2  /
//  |  /   |  /
//  | /    | /
//5 .------.1
//   interior  
  
  A=%pi/N;
  SplayAngle=atan(diff(Re)/H)
  MiterCutAngle=atan(sin(SplayAngle)*tan(A));//Miter angle
  TiltCutAngle=atan(ones(SplayAngle)*tan(A).*cos(atan(tan(SplayAngle)*(1./cos(A)))))
  if argn(2)<5 then
    ngap=0;gap_width=0;typ="full"; filled=%f
  else
    ngap=GapProps.ngap
    gap_width=GapProps.gap
    typ=convstr(GapProps.type)
    filled=GapProps.filled
  end

  [Xh,Yh]=Segment2D(Ri(1),Re(1),N,GapProps);
  [Xl,Yl]=Segment2D(Ri(2),Re(2),N,GapProps);
  
  X=[];Y=[];Z=[];
  zs=[0      H     H     0      0      H      H       0  ];
  if ngap==0|gap_width==0|typ=="full"  then
    //Stave3D return a single "stave"

    //      1       2       3       4       5       6       7       8
    xs=[Xh(1,1) Xl(1,1) Xl(2,1) Xh(2,1) Xh(4,1) Xl(4,1) Xl(3,1) Xh(3,1)];
    ys=[Yh(1,1) Yl(1,1) Yl(2,1) Yh(2,1) Yh(4,1) Yl(4,1) Yl(3,1) Yh(3,1)];
    exec(facettes);
  else
    if ngap==N then
      //      1       2       3       4       5       6       7       8
      xs=[Xh(1,1) Xl(1,1) Xl(2,1) Xh(2,1) Xh(4,1) Xl(4,1) Xl(3,1) Xh(3,1)];
      ys=[Yh(1,1) Yl(1,1) Yl(2,1) Yh(2,1) Yh(4,1) Yl(4,1) Yl(3,1) Yh(3,1)];
      exec(facettes);
      if filled then
        //      1       2       3       4       5       6       7       8       
        xs=[Xh(1,2) Xl(1,2) Xl(2,2) Xh(2,2) Xh(4,2) Xl(4,2) Xl(3,2) Xh(3,2)];
        ys=[Yh(1,2) Yl(1,2) Yl(2,2) Yh(2,2) Yh(4,2) Yl(4,2) Yl(3,2) Yh(3,2)];
        exec(facettes);
      end
     
    else
      //segment upon space  
      //      1       2       3       4       5       6       7       8
      xs=[Xh(1,1) Xl(1,1) Xl(2,1) Xh(2,1) Xh(4,1) Xl(4,1) Xl(3,1) Xh(3,1)];
      ys=[Yh(1,1) Yl(1,1) Yl(2,1) Yh(2,1) Yh(4,1) Yl(4,1) Yl(3,1) Yh(3,1)];
      exec(facettes);
   
      //segment above space 
      //      1       2       3       4       5       6       7       8
      xs=[Xh(1,$) Xl(1,$) Xl(2,$) Xh(2,$) Xh(4,$) Xl(4,$) Xl(3,$) Xh(3,$)];
      ys=[Yh(1,$) Yl(1,$) Yl(2,$) Yh(2,$) Yh(4,$) Yl(4,$) Yl(3,$) Yh(3,$)];
      exec(facettes);
    
      
      //regular (voir si on peut le deplacer apres le spacer)
      GapProps.ngap=0
      [Xhr,Yhr]=Segment2D(Ri(1),Re(1),N,GapProps);
      [Xlr,Ylr]=Segment2D(Ri(2),Re(2),N,GapProps);
      [Xhr,Yhr]=Rot2D(Xhr,Yhr,A)
      [Xlr,Ylr]=Rot2D(Xlr,Ylr,A)
      
      //      1       2       3       4       5       6       7       8
      xs=[Xhr(1,1) Xlr(1,1) Xlr(2,1) Xhr(2,1) Xhr(4,1) Xlr(4,1) Xlr(3,1) Xhr(3,1)];
      ys=[Yhr(1,1) Ylr(1,1) Ylr(2,1) Yhr(2,1) Yhr(4,1) Ylr(4,1) Ylr(3,1) Yhr(3,1)];
      exec(facettes);
    
      if filled then
        //spacer
        xs=[Xh(1,2) Xl(1,2) Xl(2,2) Xh(2,2) Xh(4,2) Xl(4,2) Xl(3,2) Xh(3,2)];
        ys=[Yh(1,2) Yl(1,2) Yl(2,2) Yh(2,2) Yh(4,2) Yl(4,2) Yl(3,2) Yh(3,2)];
        exec(facettes);
      end
    end
  end
 
endfunction

function facettes()
  //vertices index
//    7  .------. 3
//     / |    / |
//    /  |   /  |
//   /  8|  /   |
//6 .----.-.--- .4
//  |   /  |2  /
//  |  /   |  /
//  | /    | /
//5 .------.1
//   interior  
  
//facette basse
    x=[xs(1); xs(4); xs(8);xs(5);xs(1)];
    y=[ys(1); ys(4); ys(8);ys(5);ys(1)];
    z=[    0;     0;     0;    0;    0]; 
    X=[X,x];Y=[Y,y];Z=[Z,z]; 
    //facette haute
    x=[xs(2); xs(3); xs(7);xs(6);xs(2)];
    y=[ys(2); ys(3); ys(7);ys(6);ys(2)];
    z=[    0;     0;     0;    0;    0]+H; 
    X=[X,x];Y=[Y,y];Z=[Z,z]; 
    //facette exterieure
    x=[xs(4); xs(3); xs(7);xs(8);xs(4)];
    y=[ys(4); ys(3); ys(7);ys(8);ys(4)];
    z=[    0;     H;     H;    0;    0]; 
    X=[X,x];Y=[Y,y];Z=[Z,z]; 
   
     //facette interieure
    x=[xs(1); xs(2); xs(6);xs(5);xs(1)];
    y=[ys(1); ys(2); ys(6);ys(5);ys(1)];
    z=[    0;     H;     H;    0;    0]; 
    X=[X,x];Y=[Y,y];Z=[Z,z]; 
    
    //facette A-
    x=[ xs(1); xs(2); xs(3); xs(4); xs(1)];
    y=[ ys(1); ys(2); ys(3); ys(4); ys(1)];
    z=[    0;     H;     H;      0;     0]; 
    X=[X,x];Y=[Y,y];Z=[Z,z]; 
   
    //facette A+
    x=[xs(5);xs(6);xs(7);xs(8);xs(5)];
    y=[ys(5);ys(6);ys(7);ys(8);ys(5)];
    z=[   0;    H;    H;     0;    0]; 
    X=[X,x];Y=[Y,y];Z=[Z,z]; 
endfunction
