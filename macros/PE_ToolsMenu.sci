function PE_ToolsMenu(mainH)
//Creates the File menu for the RingEditor window
  w=string(mainH.figure_id);
  Tools=uimenu("parent", mainH, "label", _("Tools"),"Handle_Visible", "on"),
  Dims = uimenu("parent"   , Tools, ..
                "label"    , _("TS","Workpiece dimensions"),...
                "callback" ,list(4,"PE_Dimensions("+w+")"))
  ShowHide =  uimenu("parent"   , Tools, ..
                "label"    , _("TS","Hide knots"),...
                "callback" ,list(4,"PE_ShowHide("+w+")"))
  SetP= uimenu("parent"   , Tools, ..
               "label"    , _("TS","Toogle profile"),...
               "callback" ,list(4,"PE_SetProfile("+w+")"))
  Pi= uimenu("parent"   , Tools, ..
               "label"    , _("TS","Show default interior profile "),...
               "callback" ,list(4,"PE_ShowDefaultProfile("+w+")"))
  Scale= uimenu("parent"   , Tools, ..
               "label"    , _("TS","Scale profiles"),...
               "callback" ,list(4,"PE_Scale("+w+")"))

endfunction
