function cotea(o,a,r)
  n=20;
  t=linspace(a(1),a(2),n);
  d=abs(diff(a))*180/%pi
  s=msprintf("%g",round(10*d)/10);
  rect=xstringl(0,0,s);w=rect(3)
  xpoly(o(1)+[0 1.5*r*cos(a(1))],o(2)+[0 1.5*r*sin(a(1))])
  xpoly(o(1)+[0 1.5*r*cos(a(2))],o(2)+[0 1.5*r*sin(a(2))])
  xpoly(o(1)+r*cos(t),o(2)+r*sin(t));gce().foreground=5;
  xpoly(o(1)+r*cos(a(1)*[1+0.0001,1]),o(2)+r*sin(a(1)*[1+0.0001,1]));gce().foreground=5;gce().polyline_style=4;
  xpoly(o(1)+r*cos(a(2)*[1 1+0.0001]),o(2)+r*sin(a(2)*[1 1+0.0001]));gce().foreground=5;gce().polyline_style=4;

 xstring(o(1)+r*cos(a(1))-w*1.2,o(2)+r*sin(a(1)),s)

endfunction
