function RE_Update(guiHandle,D)
//This function is used to update the RingEditor window when some
//parameter is changed.
 
  Handles=guiHandle.user_data.H
 
  C=guiHandle.children
  Frames=C(C.type=="uicontrol")
  STD=Frames(Frames.tag=="RE_STD")
  ZZ=Frames(Frames.tag=="RE_ZZ")
 
  
  l=Handles.index.value;
  RP=D.RingsProperties(l);
  Handles.index.string=string(1:size(D.H,"*"))';Handles.index.value=l;
  Handles.RH.string=string(D.H(l));
  Handles.N.string=string(RP.N);
  d=[0 divisors(RP.N)];
  Handles.Rot.string=string(RP.Rotation);
  Handles.scolor.value=find(rgb2hex(wood_colormap())==RP.SpacerColor);
  Handles.ngap.string=string(d);
  if or(RP.Type==["Standard" "Staves"]) then
    Handles.RingType.value=find(RP.Type==["Standard" "Zigzag" "Staves"]);
    ZZ.visible="off";
    STD.visible="on";
    GP=RP.GapProps(1);
    Handles.ngap.value=find(GP.ngap==d);
    Handles.gap.string=string(GP.gap);
    
    dsp=Handles.Display.string;
    v=Handles.Display.value;
    Handles.Display.string(Handles.Display.string==_("TS","Zigzag design"))=[];
    Handles.Display.value=v;
   
    if GP.ngap==0 then
      Handles.ngap.enable="on";
      Handles.gap.enable="off";
      //Handles.Shape.enable="off";
      Handles.Type.enable="off";
      Handles.scolor.enable="off";
    else
      Handles.ngap.enable="on";
      Handles.gap.enable="on";
      //Handles.Shape.enable="on" ;
      Handles.Type.enable="on";
      if GP.filled then
        Handles.Type.value=1;
        Handles.scolor.enable="on"
      else
        Handles.Type.value=2;
        Handles.scolor.enable="off"
      end
    end
    Handles.ApplyTo.string=string(Handles.index.value);
    Colors=RP.Colors(1);
    Handles.Color.value=find(Colors(1)==Handles.Color.string(:,1));
    Handles.nc.string=index2exp(find(Colors(1)==Colors));
  elseif RP.Type=="Zigzag" then
    Handles.RingType.value=2;
    v=Handles.Display.value;
    Handles.Display.string($+1)=_("TS","Zigzag design");
    Handles.Display.value=v;
    STD.visible="off";
    ZZ.visible="on";
    ZigzagProps=RP.ZigzagProps(1);
    ll=Handles.ZZindex.value;
    if ll==0 then ll=1;end
    if ZigzagProps.thickness<>[] then
      Handles.ZZindex.string=string(1:size(ZigzagProps.thickness,"*"))';
      Handles.ZZindex.value=ll;
      Handles.ZZColor.value=find(Handles.ZZColor.string(:,1)==ZigzagProps.colors(ll))
    end
    Handles.zheight.string=string(ZigzagProps.zheight);
    Handles.thickness.string=sci2exp(ZigzagProps.thickness);
    
  end
  guiHandle.user_data.D=D
 
  
  fn=Handles.Display.string(Handles.Display.value)
  guiHandle.immediate_drawing="off"
  ax=C(C.type=="Axes")
  delete(ax);ax=gca()
  ax.axes_bounds=[0.35 0 0.65 1];
  
  select fn
  case _("TS","Raw Ring 2D")
    SegmentedRing2D(D,l,%t);
  case _("TS","Raw Ring 3D")
    SegmentedRing3D(D,l,%t);
  case _("TS","Turned Ring 3D")
    Ring3D(D,l,%t);
  case _("TS","2D view")
    TS_2D(D)
  case _("TS","Raw workpiece")
    TS_3D(D,%f,%t)
  case _("TS","Turned workpiece")
    TS_3D(D,%t,%t)
  case _("TS","Unrolled")
    RE_Unroll(D)
  case _("TS","Element sketch")
    RE_Sketch(D,l)
  case _("TS","Zigzag design")
    ZZ_Design(D,l)
  end
 guiHandle.immediate_drawing="on"

endfunction
