function [MiterCutAngle,TiltCutAngle]=StaveAngles(N,SplayAngle)
//N:numberofstaves
//SplayAngle: Outward tilt angle of staves (degrees)
//https://suncatcherstudio.com/woodturning/segmented-wood-turning/
  A=%pi./matrix(N,1,-1);
  MiterCutAngle=atan(sin(SplayAngle).*tan(A));//Miter angle
  TiltCutAngle1=atan(ones(SplayAngle)*tan(A).*cos(atan(tan(SplayAngle)*(1./cos(A)))));
  w=sin(A).*cos(SplayAngle);
  TiltCutAngle=(atan(w./sqrt(1-w.^2)));
endfunction
