function PE_ShowHide(win)
  //Toggle knots visualization
  if type(win)==1 then
    mainH=findobj("figure_id", win)
  else
    mainH=win
  end
  C=mainH.user_data.C
  if C(1).visible=="on" then
    C(1).visible="off"    
    mainH.children(1).children(4).Label=_("TS","Show knots")
  else
    C(1).visible="on"
    mainH.children(1).children(4).Label=_("TS","Hide knots")
  end
endfunction
