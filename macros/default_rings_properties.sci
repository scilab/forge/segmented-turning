function rp=default_rings_properties(Nrings)
  //creates the defaut RingProperties data structure
  N=[]
  c=list();
  g=list();
  r=[];
  s=[];
  t=[];
  zz=list();
  st=list();
  Nsegs=12;
  for k=1:Nrings
    N=[N,Nsegs];
    g($+1)=struct("ngap",0,"gap",0,"type","full","filled",%f);
    c($+1)="#C68E44"+emptystr(1,Nsegs);
    s($+1)="#A06823"
    r($+1)=0.5;
    t($+1)="Standard";
    zz($+1)=struct("zheight",0,"thickness",[],"colors",[])
    st($+1)=struct("Re",[],"Ri",[])
  end
   rp=mlist(["rp","N","Rotation","Type","Colors","SpacerColor","GapProps" "ZigzagProps","StavesProps"],N,r,t,c,s,g,zz,st);
endfunction
