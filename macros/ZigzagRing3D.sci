function   [S,M]=ZigzagRing3D(N,Ri_z,Re_z,z,RP)
  X=[];Y=[];Z=[];Col=[];
  zigzagprops=RP.ZigzagProps(1);
  if zigzagprops.thickness==[] then return;end
  A=%pi/N;
  [S,M]=ZigzagSegment3Dt(Ri_z,Re_z,z,A,zigzagprops);
  
  X=[];Y=[];Z=[];Col=[];
  Xm=[];Ym=[];Zm=[];
  x=S.X;y=S.Y;z=S.Z;c=S.C;
  xm=M.X;ym=M.Y;zm=M.Z;
  a=0;
  for k=1:N
    [x,y]=Rot2D(x,y,a,[0,0]);
    X=[X x];Y=[Y y];Z=[Z z];Col=[Col c];
    
    [xm,ym]=Rot2D(xm,ym,a,[0,0]);
    Xm=[Xm xm];Ym=[Ym ym];Zm=[Zm zm];
    
    a=2*A;
  end
  S=list(struct("X",X,"Y",Y,"Z",Z,"C",Col))
  M=list(struct("X",Xm,"Y",Ym,"Z",Zm))
endfunction
