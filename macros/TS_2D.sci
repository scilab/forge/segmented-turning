function TS_2D(D)
//Shows a vertical cut of the workpiece with the raw segments as well
//as the interior and exterior profiles of the turned workpiece
  RP=D.RingsProperties;
  N=RP.N(:)'
  A=%pi./N;

  ax=gca();delete(ax.children);
  ax.isoview="on";
  H=D.H;h=cumsum(H);
  kr=find(RP.Type<>"Staves")
  if kr<>[] then
    Re=D.Re(kr)+D.Ewidth;
    Ri=max(0,D.Ri(kr).*cos(A(kr))-D.Ewidth);//;
    R=[Ri;h(kr);Re-Ri;H(kr)];
    xrects(R,-5*ones(Re));
    R(1,:)=-Re
    xrects(R,-5*ones(Re));
    xnumb(-Ri,h(kr)-H(kr)/2,kr)
  end
  
  ks=find(RP.Type=="Staves")
  for k=ks
    Hk=H(k);
    StavesProps=RP(k).StavesProps(1);
   
    Re=StavesProps.Re+D.Ewidth;
    Ri=StavesProps.Ri-D.Ewidth./cos(A(k));
    e=Re(1)-Ri(1)
    
    [SplayAngle,MiterCutAngle,TiltCutAngle]=StaveAngles(A(k),diff(Re),Hk);
    xe=Re;
    xi=Ri*cos(A(k));
    dx=max(xe-xi)
    xi=xe-dx; //OK
    
    X=[xi(1);xe(1);xe(2);xi(2);xi(1)];
    Y=sum(H(1:k-1))+[0;0;Hk;Hk;0];
    xpolys([X,-X],[Y Y],[5 5])
    
    xnumb(-(X(1)+X(2))/2,Y(1)+H(k)/2,k)
  end
 
  ax.data_bounds=[-max(Re)-10 -2;max(Re)+10,sum(H)+10]
  ax.tight_limits=["on","on"];
  //Exterior profile
  Pe=D.Pe;
  plot(Pe.x,Pe.y); plot(-Pe.x,Pe.y);
  //Interior profile 
  Pi=D.Pi;
  plot(Pi.x,Pi.y,'r');plot(-Pi.x,Pi.y,'r');
endfunction

