function [r,v]=CheckValueInSet(s,h)
//Checks the answer given in Edit uicontrol
//It should be evaluated as a real scalar in the set given by the first argument 
  if argn(2)==1 then h=gcbo;end
  if execstr("v=("+h.string+")","errcatch")<>0 then
    RE_SetError(h,%t,_("TS","Expression cannot be evaluated"))
    r=%f,v=[]
  elseif type(v)<>1|size(v,"*")<>1|~isreal(v) then
    RE_SetError(h,%t,_("TS","Real scalar expected"))
    r=%f
  else
    if argn(2)<1 then
      RE_SetError(h,%f)
      r=%t
    else
      r=or(v==s)
      
      if r then
        RE_SetError(h,~r)//un hilite edit area
      else
        msg=msprintf(_("TS","Expected value must be in the set %s"),sci2exp(s))
        RE_SetError(h,~r,msg)
      end
    end
  end
endfunction
