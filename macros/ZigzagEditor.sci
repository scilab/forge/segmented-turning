function  ZigzagProps=ZigzagEditor(mainH,h,l);
  margin_x     = 5;      // Horizontal margin between each elements
  margin_y     = 5;      // Vertical margin between each elements
  button_w     = 140;
  button_h     = 30;
  label_h      = 25;
  label_w      = 180;
  bg=color("lightgray");
  defaultfont  = "arial"; 
  guiHandle = figure("menubar", "none", ...
                     "toolbar","none", ...
                     "infobar_visible", "on",  ...
                     "axes_size",[900 600],...
                     "figure_name",_("TS","Zigzag editor")+" (%d)",...
                     "visible","off",...
                     "color_map",wood_colormap(),...
                     "tag","ZZEditor");
  guiHandle.closerequestfcn="ZZ_Close("+string(guiHandle.figure_id)+")";
  guiHandle.background=color("lightgray")
  ax=gca();
  ax.axes_bounds=[0.35 0 0.75 1];
  ZZ_FileMenu(guiHandle)
    frame_border=createBorder("titled",createBorder("etched"),...
                            _("TS","Laminated board properties"),...
                            "left","top",createBorderFont("Sans Serif",12,"bold"),"blue")
  wf1=310;hf1=240;
  xf1=margin_x;yf1=600-hf1-4*margin_y;
  f1=uicontrol( ...
      "parent"              , guiHandle,...
      "style"               , "frame",...
      "position"            , [xf1 yf1 wf1 hf1],....
      "fontsize"            , 12,...
      "fontWeight"          , "bold",...
      "background"          , 0.85*ones(1,3),...
      "border"              , frame_border);
 
 
  H=[];
   x=margin_x;
  y=hf1-4*margin_y-label_h;
  CreateLabel(f1,x,y,label_w,label_h,_("TS","Zigzag height mm"));
  x=x+margin_x+label_w;
  H.zheight=CreateEdit(f1,x,y,label_w,label_h,string(height),"zheight","ZZ_callback()");
   
  x=margin_x;
  y=y-margin_y-label_h;
  CreateLabel(f1,x,y,label_w,label_h,_("TS","Layers thickness mm"));
  x=x+margin_x+label_w;
  H.thickness=CreateEdit(f1,x,y,label_w,label_h,"[]","thickness","ZZ_callback()");
 
  x=margin_x;
  y=y-margin_y-label_h;
  CreateLabel(f1,x,y,label_w,label_h,_("TS","Layer index"));
  x=x+margin_x+label_w;
  H.index=CreateEdit(f1,x,y,30,label_h,"1","index","ZZ_callback()");
 
  x=margin_x;
  y=y-margin_y-label_h;
  CreateLabel(f1,x,y,0.7*label_w,label_h,_("TS","Layer wood"));
  x=x+margin_x+0.7*label_w;
  [C,Cn]=wood_colormap()
  hc=rgb2hex(C);
  choices=[hc,Cn,hc]
  H.Color=CreatePopup(f1,x,y,0.9*label_w,label_h,choices,"color","ZZ_callback()");
  Layers=struct("zheight",20,"thickness",[],"colors",[])
  guiHandle.user_data=struct("H",H,"Layers",Layers,"Segments",[h,l],"Path","Untitled.zz","Edited",%f); 
guiHandle.visible="on";
endfunction
