function RE_Export(win)
//Filemenu callback for the "Export display" sub menu  
//It generates a Scilab graphic figure clone of the RingEditor display
  if type(win)==1 then
     mainH=findobj("figure_id", win)
  else
    mainH=win
  end
  c=["pdf";"svg";"jpeg";"png";_("TS","graphic window")];
  n=x_choose(c,_("TS","Give export file format"));
  if n==0 then return;end
  
  D=mainH.user_data.D
  H=mainH.user_data.H
  fn=H.Display.string(H.Display.value)
  l=evstr(H.index.value)
  pdf=%t
  if n<>5 then
    path=uiputfile("*."+c(n),"",msprintf(_("TS","Give export file path")));
    if path=="" then return;end
    driver(convstr(c(n),'u'));
    xinit(path)
  end

  fig=scf(max(winsid())+1);
  drawlater()
  
  select fn
  case _("TS","Raw Ring 2D"),
    if c(n)=="pdf" then
      //insure 
      px2cm=get(0,"screensize_cm")./get(0,"screensize_px");
      px2mm=px2cm(3)*10;
      [X]=SegmentedRing2D(D,l,%t);
      sz=max(X)
      f=gcf();f.axes_size=(2*sz/px2mm)*[1 1];
      ax=gca();
      ax.margins=zeros(1,4);
      ax.tight_limits="on";
      ax.data_bounds=sz*[-1 -1;1 1];
      ax.axes_visible="off";
    else
      SegmentedRing2D(D,l,%t);
    end
  case _("TS","Raw Ring 3D")
    SegmentedRing3D(D,l,%t);
  case _("TS","Turned Ring 3D")
    Ring3D(D,l,%t);
  case _("TS","2D view")
    TS_2D(D)
  case _("TS","Raw workpiece")
     TS_3D(D,%f,%t)
  case _("TS","Turned workpiece")
    TS_3D(D,%t,%t)
  case _("TS","Unrolled")
    RE_Unroll(D)
  case _("TS","Zigzag design")
    ZZ_Design(D,l)
  end
  fig.background=color("lightgray");
  drawnow()
  if n<>5 then
    xend()
    driver("Rec")
  end
scf(mainH)
endfunction
