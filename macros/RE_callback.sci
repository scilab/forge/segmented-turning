function RE_callback()
//Callbacks of all user input RingEditor uicontrols
//The handles of input uicontrols  are stored in an array H which can be
//retreived in the RingEditor figure user_data
  guiHandle=gcbo.parent
  while guiHandle.type<>"Figure" then guiHandle=guiHandle.parent;end
  
  ud=guiHandle.user_data
  D=ud.D
  if D==[] then return,end
  Handles=ud.H //[l,Rot,ngap,gap,Shape,Type,Color,N,Display]
  Edited=ud.Edited
  l=Handles.index.value
  select gcbo.tag
  case "index"
    l=Handles.index.value

  case "RH" then
    [r,h]=CheckNumValue([0 sum(D.H)])
    if ~r then return;end
    Heights=D.H;
    Hl=Heights(l);
    if Hl==h then return;end
    Heights(l)=h;
    mxy=max(D.Pe.y)
    if sum(Heights)<mxy then
      messagebox(msprintf(_("TS","Sum of ring heights (%g mm) is less than workpiece height (%g mm)"),..
                        sum(Heights),round(max(D.Pe.y))),"warning","warning","modal")
    else
      nh=size(Heights,"*")
      k=find(cumsum(Heights)>=mxy,1)
      if sum(Heights(1:k-1))<mxy then k=k+1;end
      if k<=nh then
       messagebox(msprintf(_("TS","%d top rings are upon the workpiece"),nh-k+1),...
                     "warning","warning","modal")

      end
    end
    D.H=Heights
    D=Profile2Rays(D)
    Edited=%t
  case "N"
    [r,N]=CheckNumValue([4 %inf])
     if ~r then return;end
     RP=D.RingsProperties(l)
     if RP.N==N then return;end
     RP.N=N
     RP.Colors(1)=RP.Colors(1)(1)+emptystr(1,N)
     D.RingsProperties(l)=RP
     if RP.Type=="Staves" then D=StavesRays(D,l);end
     Edited=%t
  case "ngap"
    RP=D.RingsProperties(l)
    h=gcbo
    ngap=evstr(h.string(h.value))
    //if ~r then return;end
    GP=RP.GapProps(1)
    if GP.ngap==ngap then return;end
    if ngap<>0 then
      //GP.type=Handles.Shape.string(Handles.Shape.value)
      GP.filled=Handles.Type.value==1
      GP.type="rectangle"
    else
      GP.type="full"
    end
    GP.ngap=ngap
    GP.gap=evstr(Handles.gap.string)
    RP.GapProps=GP
    D.RingsProperties(l)=RP
    Edited=%t
  case "gap"
    [r,gap]=CheckNumValue([0 %inf])
    if ~r then return;end
    RP=D.RingsProperties(l)
    GP=RP.GapProps(1)
    if GP.gap==gap then return,end
    GP.gap=gap
    RP.GapProps(1)=GP
    D.RingsProperties(l)=RP
    Edited=%t;

  case "Type"
    RP=D.RingsProperties(l)
    GP=RP.GapProps(1)
    if Handles.Type.value==1==GP.filled  then return;end
    if Handles.Type.value==1 then
      GP.filled=%t
    else
      GP.filled=%f
    end
    RP.GapProps(1)=GP
    D.RingsProperties(l)=RP
    Edited=%t
  case "scolor"
    RP=D.RingsProperties(l)
    c=Handles.scolor.string(Handles.scolor.value,1)
    if RP.SpacerColor==c then return;end
    RP.SpacerColor=c
    D.RingsProperties(l)=RP 
    Edited=%t
  case "Rot"
    [rt,r]=CheckNumValue([-1 1])
    if ~rt then return;end
    RP=D.RingsProperties(l)
    if RP.Rotation==r; then return;end
    RP.Rotation=r;
    D.RingsProperties(l)=RP
    Edited=%t
  case "RingType"
    RP=D.RingsProperties(l)
    Types=["Standard" "Zigzag" "Staves"];
    
    RP.Type=Types( Handles.RingType.value);
    D.RingsProperties(l)=RP
    if RP.Type=="Staves" then D=StavesRays(D,l);end
   
  case "color" 
    RP=D.RingsProperties(l)
    c=Handles.Color.string(Handles.Color.value,1)
    ind=evstr(Handles.nc.string)
    RP.Colors(1)(ind)=c
    D.RingsProperties(l)=RP
  case "nc"
     RP=D.RingsProperties(l)
    [r,ind]=CheckIntArray([1 RP.N])
    if ~r then return;end
    c=Handles.Color.string(Handles.Color.value,1)
    RP=D.RingsProperties(l)
    C=RP.Colors(1)
    C(ind)=c
    if and(C==RP.Colors(1)) then return;end
    RP.Colors(1)=C
    D.RingsProperties(l)=RP
    Edited=%t
  case "ApplyTo"
    [r,ind]=CheckIntArray([1 size(D.H,"*")])
    if ~r then return;end
    if and(ind==l) then return,end
    for i=matrix(ind,1,-1)
       D.RingsProperties(i)=D.RingsProperties(l)
    end
  case "Display"
  case "zheight"
    Height=D.H(l);
    RP=D.RingsProperties(l)
    ZigzagProps=RP.ZigzagProps(1)
    [r,v]=CheckZheight(Height)
    if ~r then return;end
    if ZigzagProps.zheight==v then return;end
    ZigzagProps.zheight=v; 
    RP.ZigzagProps(1)=ZigzagProps
    D.RingsProperties(l)=RP
  case "thickness"
    RP=D.RingsProperties(l)
    ZigzagProps=RP.ZigzagProps(1)
    [r,thick]=CheckThickness(ZigzagProps,RP.N,D.H(l),D.Re(1,l))
    if ~r then return;end
    prevthick=ZigzagProps.thickness
    if and(prevthick==thick)  then return;end
    n=size(thick,"*")
    np=size(prevthick,"*")
    c=ZigzagProps.colors
    [C,Cn]=wood_colormap()
    defcolor=rgb2hex(C(1,:));
    if np<n then 
      c(np+1:n)=defcolor;
    elseif np>n then 
      c=c(1:n);
    end 
    ZigzagProps.thickness=thick
    ZigzagProps.colors=c
    RP.ZigzagProps(1)=ZigzagProps
    D.RingsProperties(l)=RP
  case "ZZindex"
    l=Handles.index.value
    RP=D.RingsProperties(l)
    ZigzagProps=RP.ZigzagProps(1)
    if ZigzagProps.colors<>[] then
      ll=Handles.ZZindex.value
      [C,Cn]=wood_colormap()
      Handles.ZZColor.value=find(rgb2hex(C)==ZigzagProps.colors(ll))
    end
    return
  case "ZZcolor"
    RP=D.RingsProperties(l)
    ZigzagProps=RP.ZigzagProps(1)
    ll=Handles.ZZindex.value
    if ll>0 then
      if ZigzagProps.colors(ll)==Handles.ZZColor.string(Handles.ZZColor.value,1) then return;end
      ZigzagProps.colors(ll)=Handles.ZZColor.string(Handles.ZZColor.value,1)  
      RP.ZigzagProps(1)=ZigzagProps
      D.RingsProperties(l)=RP
    end

  end
  
  guiHandle.user_data.Edited=Edited
  RE_Update(guiHandle,D)
endfunction
