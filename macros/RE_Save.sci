function RE_Save(win,opt)
//Filemenu callback for the "Save" and "Save As ..." sub menus  
//It is used to save the current workpiece daa structure

  if type(win)==1 then
     mainH=findobj("figure_id", win)
  else
    mainH=win
  end
  D=mainH.user_data.D
  
  if argn(2)<2|opt then
    path=uiputfile("*.ts")
    if path=="" then return,end
  else
    path=mainH.user_data.Path
  end
 
  save(path,"D")
  mainH.user_data.Edited=%f
  mainH.user_data.Path=path;
endfunction
