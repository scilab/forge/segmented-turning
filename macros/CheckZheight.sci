function [r,v]=CheckZheight(height)
  if argn(2)==1 then h=gcbo;end
  if execstr("v=(["+h.string+"])","errcatch")<>0 then
    RE_SetError(h,%t,_("TS","Expression cannot be evaluated"))
    r=%f,v=[]
  elseif type(v)<>1|~isreal(v)|or(v<=0) then
    RE_SetError(h,%t,_("TS","positive number  expected"))
    r=%f
  else
  r=v<=height
   if r then
     RE_SetError(h,~r)//un hilite edit area
   else
     msg=msprintf(_("TS","%s exceeds ring height (%g)"), "Zigzag height",...
                   height)
     RE_SetError(h,~r,msg)
   end
  end
endfunction
