mode(-1)
help_lang_dir = get_absolute_file_path("build_help.sce");
xmlfiles=[listfiles(help_lang_dir+"*.xml")]
if newest([help_lang_dir+"date_build";xmlfiles])==1 then
  clear  xmlfiles lang
  return
end
tbx_build_help("SegmentedTurning", help_lang_dir);
mputl(sci2exp(getdate()),help_lang_dir+"date_build")
clear help_lang_dir;
