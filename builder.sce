mode(-1);
function main_builder()
  try
    getversion("scilab");
  catch
    error(_("Scilab 6.0 or more is required."));
  end;

  if ~with_module("development_tools") then
    error(msprintf(_("%s module not installed."),"development_tools"));
  end
 
  TOOLBOX_NAME = "TS"
  TOOLBOX_TITLE = _("SegmentedTurning");

  toolbox_dir = get_absolute_file_path("builder.sce");

  tbx_builder_macros(toolbox_dir);
 
  pathmacros=toolbox_dir+"macros"
  TSlib = lib(toolbox_dir+"macros");
  
  UpdatePoFiles(toolbox_dir)
  tbx_build_localization(toolbox_dir);
 
  tbx_builder_help(toolbox_dir);
  tbx_build_loader(toolbox_dir);
  tbx_build_cleaner(toolbox_dir);
 
endfunction
function UpdatePoFiles(toolbox_dir)
  path=toolbox_dir
  lpath=path+"/locales"
  en_pofile=tbx_generate_pofile(path);
  //Modify en_US.po header
 
  h=mgetl(lpath+"/head");
  txt=mgetl(en_pofile);
  txt=[h;txt(21:$)];

  mputl(txt,en_pofile)
 //merge all po files except en_US.po
  pofiles=ls(lpath+"/*.po");
  pofiles(pofiles==lpath+"/en_US.po")=[];
  if getos() == "Windows" then
    cmd = SCI + filesep() + "tools/gettext/msgmerge";
  else
    cmd = "msgmerge";
  end
  for pofile=pofiles'
    mputl(txt,TMPDIR+"/tmp.po")
    [rep, status, msgerr]=unix_g(cmd+" "+pofile+" "+TMPDIR+"/tmp.po --output-file="+pofile)
    if status<>0 then mprintf("%s\n",msgerr);end
    if %f then
      T=mgetl(pofile)
      k=find(T(18:$)=="msgstr """"")
      if k<>[] then 
        msprintf("%s\n",T(17+k-1))
        warning("Missing translation in "+pofile);
      end
    end
  end
endfunction

main_builder()

clear main_builder
