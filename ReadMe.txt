This Scilab (www.scilab.org) toolbox provides mainly two interactive 
tools for designing segmented turning workpieces.

The first one (ProfileEditor) allow to draw the  exterior and interior
 profiles of the workpiece.

The second one (RingEditor) allow to specify all properties of rings
 and segments like heights, number of segments, spaces or spacers,
 type of wood,... and finally to generate a spreadsheet giving
 characteristics of all cuts needed to build workpiece.


Both tools have been designed to be easy to use without any skill
about Scilab. Help pages as well are provided to help you getting  started.

This toolbox exploits the powerfull computational and graphic
capabilities of the Scilab software.

The toolbox is licensed under the terms of the GNU GPL v2.0 so it is
free and open source.

Advices, bug reports are welcome.

Serge Steer, (Serge.Steer@laposte.net)
